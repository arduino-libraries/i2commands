/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>
#include <CommandHandler.h>

/* Create a variable of the CommandHandler. */
CommandHandler *_cmdHandler;

/* This is the function that will be called whenever this device receives a command from another device. 
In this case we will print the incomming command and also extract the vallue that has been passed along with it. */
void processCommand(Command* cmd){
    /* On this device, we are currently not expecting to receive any data. */ 
}

void setup()
{
  Serial.begin(9600);

  /* The CommandHandler is a singleton that you can just ask to pass it's instance wherever you need it.
  In this case we'll bind it tho the _cmdHandler variable. */
  _cmdHandler = CommandHandler::getInstance();

  /* To configure the CommandHandler, first we will pass it the i2c-address that this device will use to identify itself.
  In this case we defined DEVICE_B in Settings.h but you can configure it any way you like. 
  The second argument refers to the function that you will use to process any command received by this device. */
  _cmdHandler->configure(ADDRESS_DEVICE_B, processCommand);

  delay(3000); // Waiting 3 seconds to let the other device(s) initialize too.
  Serial.println("Setup complete(device B)!");
}

void loop()
{
  /* Every time loop() gets executed, pending commands will be transmitted to their intended recipients. 
  This is handled by the run() method of the CommandHandler.
  In this case, we are sending the number 1337 every 2 seconds. */
  _cmdHandler->send(new Command(ADDRESS_DEVICE_A, CMD_PRINT_NUMBER, "1337"));

  _cmdHandler->send(new Command(ADDRESS_DEVICE_A, CMD_PRINT_GREETING, "Arjen"));
  _cmdHandler->run();
  Serial.println("Commands delivered!");

  delay(5000);
}
