/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CommandHandler_H
#define CommandHandler_H

#include <Arduino.h>
#include <Wire.h>
#include <CommandBuffer.h>
#include <ICommandListener.h>
#include <Command.h>
#include <CommandCodes.h>

extern int CURRENT_DEVICE_ADDRESS;

class CommandHandler
{
public:
  static CommandHandler *getInstance();
  void run();
  void configure(int DEVICE_ADDRESS, ICommandListener *listener);
  void configure(int DEVICE_ADDRESS, void (*)(Command *)); // Static callback
  void send(Command *command);
  static void receiveCommand(int howMany);
  
  ICommandListener *getListener();

private:
  static CommandHandler *_instance;
  CommandHandler();
  void transmitCommands();

  CommandBuffer _cmdBuffer;
  ICommandListener *_listener = NULL;
  static void (*_staticProcessCommand)(Command *) = NULL;
  
};

#endif
