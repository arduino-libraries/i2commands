/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CommandBuffer_H
#define CommandBuffer_H

#include <Arduino.h>
#include <Command.h>

#define BUFFER_SIZE 20

class CommandBuffer
{
public:
  void clear();
  void remove(int index);
  void add(Command *cmd);
  Command *get(int index);
  int size();
  bool isEmpty();

private:
  Command *_commandBuffer[BUFFER_SIZE] = {NULL};
  int findFirstFreeSlot();
};

#endif
