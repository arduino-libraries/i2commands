/* Copyright (C) 2018  Arjen Stens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <CommandHandler.h>

/*********************************************************************
 * Represents the I2C addres of the microcontroller implementing this library.
 * This won't change after configure() has been called.
*********************************************************************/
int CURRENT_DEVICE_ADDRESS;

CommandHandler* CommandHandler::_instance = NULL;
void (*CommandHandler::_staticProcessCommand)(Command*);

CommandHandler* CommandHandler::getInstance(){
    if (_instance == NULL){ // prevents double initialization
        _instance = new CommandHandler();
    }
    return _instance;
}

CommandHandler::CommandHandler(){}

/*********************************************************************
 * Actually transmits commands to their recipients.
 * should be at least called once every loop()
*********************************************************************/
void CommandHandler::run() {
  transmitCommands();
}

/*********************************************************************
 * Returns listener of commandhandler. 
 * Enables other parts of the libarary to communicate with the user-defined listener.
*********************************************************************/
ICommandListener* CommandHandler::getListener(){
  return _listener;
}

/*********************************************************************
 * Adds command to buffer which will be processed in the transmitcommands() method.
*********************************************************************/
void CommandHandler::send(Command* cmd){
  _cmdBuffer.add(cmd);
}

/*********************************************************************
 * Should be called in setup().
 * It binds the current device-addres (which will be passed with the outgoing commands).
 * Also defines the object where incomming commands will be passed to (listener).
*********************************************************************/
void CommandHandler::configure(int DEVICE_ADDRESS, ICommandListener* listener){
  CURRENT_DEVICE_ADDRESS = DEVICE_ADDRESS;
  _listener = listener;

  Wire.begin(CURRENT_DEVICE_ADDRESS);
  Wire.onReceive(&CommandHandler::receiveCommand);
}

void CommandHandler::configure(int DEVICE_ADDRESS, void (*method)(Command*)){
  CURRENT_DEVICE_ADDRESS = DEVICE_ADDRESS;
  _staticProcessCommand = method;

  Wire.begin(CURRENT_DEVICE_ADDRESS);
  Wire.onReceive(&CommandHandler::receiveCommand);
}

/*********************************************************************
 * This method will be called by the built-in Wire library.
 * It saves the incomming data as an command and calls the listener's procesCommand method.
*********************************************************************/
void CommandHandler::receiveCommand(int howMany) {
  // Save incomming data as string.
  String input;
  while(Wire.available()) {
    input += (char) Wire.read();
  }

  Command* cmd = new Command(input); // Create Command from String
  if (getInstance()->getListener() != NULL)
  {
    getInstance()->getListener()->processCommand(cmd);
  }
  else {
    _staticProcessCommand(cmd);
  }
  
  delete cmd; // Object MUST be destructed.
}

/*********************************************************************
 * Sends all commands in the commandBuffer to their recipients and deletes the commands afterwards.
*********************************************************************/
void CommandHandler::transmitCommands() {
  for(int i; i < _cmdBuffer.size(); i++){// Loops until NULL value found.
    if(_cmdBuffer.get(i) != NULL){
      /* Start transmission to recipient defined in command object. */
      Wire.beginTransmission(_cmdBuffer.get(i)->getRecipient());
      Wire.write(_cmdBuffer.get(i)->toString().c_str());
      Wire.endTransmission();
      /* Command is now transmitted. */

      /* Delete sent command from commandBuffer. */
      _cmdBuffer.remove(i);

    }
    else{ // Break for-loop if last command has been transmitted.
      break;
    }
  }
}
